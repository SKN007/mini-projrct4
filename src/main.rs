use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng; 
use serde::Deserialize;

async fn project_info() -> impl Responder {
    let mut rng = rand::thread_rng();
    let numbers: Vec<i32> = (0..10).map(|_| rng.gen_range(1..=100)).collect();
    let sum: i32 = numbers.iter().sum();
    let average: f32 = sum as f32 / numbers.len() as f32;

    let response = format!(
        "Kunning Shen Project 4\nRandom Array: {:?}\nAverage: {:.2}",
        numbers, average
    );

    HttpResponse::Ok().body(response)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().route("/", web::get().to(project_info)))
    .bind("0.0.0.0:8080")?
    .run()
    .await
}


