# Mini projrct 4

## Requiements

Containerize simple Rust Actix web app

Build Docker image

Run container locally

## Steps

### Environment setup

1. Download [Docker Desktop](https://www.docker.com/products/docker-desktop/)
2. Create a Rust Actix web app
   ```
   cargo new week4_project
   ```
3. Write code in src/main.rs. The function of this website is to generate a vector randomly and calculate the mean of the vactor.
   ```
   use actix_web::{web, App, HttpResponse, HttpServer, Responder};
   use rand::Rng; 
   use serde::Deserialize;

   async fn project_info() -> impl Responder {
       let mut rng = rand::thread_rng();
       let numbers: Vec<i32> = (0..10).map(|_| rng.gen_range(1..=100)).collect();
       let sum: i32 = numbers.iter().sum();
       let average: f32 = sum as f32 / numbers.len() as f32;

       let response = format!(
           "Kunning Shen Project 4\nRandom Array: {:?}\nAverage: {:.2}",
           numbers, average
       );

       HttpResponse::Ok().body(response)
   }

   #[actix_web::main]
   async fn main() -> std::io::Result<()> {
       HttpServer::new(|| App::new().route("/", web::get().to(project_info)))
       .bind("0.0.0.0:8080")?
       .run()
       .await
   }

   ```
4. Create a Dockerfile and Makefile.
5. Open Docker Desktop. In terminal, run
   ```
   docker build -t week4_project .
   run docker run -d -p 8080:8080 week4_project
   ```

## Screenshot Display

Open Localhost:8080, we can see

![img](website.png)


In Docker Desktop, we can see

![img](docker.png)
